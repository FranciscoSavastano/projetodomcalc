//Calcula a media aritimeta dado um valor
var notas = []
var soma = 0

document.getElementById('calcularnota').addEventListener('click', function() {
    var nota = document.getElementById('inputnotas').value;
    nota = nota.replace(',', '.');
    
});

function calcularNota(){

    let nota = document.getElementById('notas').value;
    nota = nota.replace(/,/g, '.');
    if (nota === '') {
        alert('Por favor, insira uma nota');
    } else if (isNaN(nota)) {
        alert('A nota digitada é inválida, por favor, insira uma nota válida.');
    } else if (nota < 0 || nota > 10) {
        alert('A nota digitada é inválida, por favor, insira uma nota válida.');
    } else {
        let nota = document.getElementById('notas').value;
        nota = nota.replace(/,/g, '.');
        nota = parseFloat(nota)
        notas.push(nota)
        soma = notas.reduce((total, value) => total + value, 0);
        console.log(notas)
        console.log(soma)
        //Criação de elementos DOM

        var calcArea = document.getElementById("notasArea");
        var calcDiv = document.createElement("div");
        var calcText = document.createElement('p');
        calcText.textContent = "A nota " + notas.length + " foi: " + nota
        calcDiv.appendChild(calcText);
        calcArea.appendChild(calcDiv);

    }
}

function calcularMedia() {
    
    console.log(notas)

    let media = soma / notas.length;

    //Exibir media
    console.log(media);
    //Mostrar no HTML
    var resultArea = document.getElementById("resultArea");
    var resultDiv = document.createElement("div");

    var resultText = document.getElementById('resultText');
    // Se P nao existir, crie um novo
    if (!resultText) {
        resultText = document.createElement('p');
        resultText.id = 'resultText';
        resultDiv.appendChild(resultText); 
    }
    //Altere o texto, se ja existir, atualize o texto
    resultText.textContent = "A média é: " + parseFloat(media.toFixed(2))
    resultText.style.marginLeft = "1rem"
    resultDiv.appendChild(resultText);
    resultArea.appendChild(resultDiv);



}
